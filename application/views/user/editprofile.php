<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
    <meta name="theme-color" content="#ffcb05" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style-profile.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/slick/slick-theme.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:500&display=swap" rel="stylesheet">

    <title><?php echo  $judul; ?></title>
</head>

<body>

    <!-- body div -->

    <div class="app">
        <div class="app-container">

            <!-- navbar -->
            <nav class="navbar-login">
                <div class="tombol-back">
                    <a href="<?= base_url('user/profile'); ?>"><i class="fas fa-arrow-left"></i></a>
                    <span class="judul-page">My Profile</span>
                </div>
            </nav>
            <!-- akhir navbar -->

            <div class="jumbotron  text-center">
                <div class="container">
                    <img src="<?= base_url('assets/img/profile/') . $tb_customer['photo']; ?>" class="img-profile rounded-circle" alt="my-profile">
                    <h6 class="nama-user"><?= $tb_customer['app_name']; ?></h6>
                    <p class="no-telp">No Telepon : <?= $tb_customer['no_telp']; ?></p>
                    <p class="date-created">ID : <?= $tb_customer['id_customer']; ?></p>
                </div>
            </div>
            <?= $this->session->flashdata('message'); ?>
            <?= form_open_multipart('user/editprofile'); ?>
            <div class="container">
                <div class="form-group row">
                    <label for="ubahProfile" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" value="<?= $tb_customer['email']; ?>" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ubahProfile" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" value="<?= $tb_customer['app_name']; ?>"><?= form_error('name', ' <small class="text-danger" style="font-family:roboto;" role="alert">', '</small>');  ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="ubahProfile" class="col-sm-2 col-form-label">No Telp</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="telp" name="telp" value="<?= $tb_customer['no_telp']; ?>">
                        <?= form_error('telp', ' <small class="text-danger" style="font-family:roboto;" role="alert">', '</small>');  ?>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="form-group row">
                            <label for="changeFotoProfile" class="col-sm-2 col-form-label mb-2">Ganti Foto Profile</label>
                            <div class="col-sm-8">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image" name="image">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                    <img src="<?= base_url('assets/img/profile/') . $tb_customer['photo']; ?>" class="img-thumbnail shadow mb-5 mt-2" style="width:100px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <br>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-warning">Update</button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <a href="" type="btn" class="btn btn-warning ml-2">Batalkan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </form>


            <!-- akhir body div -->

            <!-- Optional JavaScript -->
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->

            <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
            <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
            <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

            <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/slick/slick.js" type="text/javascript" charset="utf-8"></script>

            <script>
                $('.custom-file-input').on('change', function() {
                    let fileName = $(this).val().split('\\').pop();
                    $(this).next('.custom-file-label').addClass("selected").html(fileName);
                });
            </script>

</body>

</html>