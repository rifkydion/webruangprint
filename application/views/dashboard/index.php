<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
    <meta name="theme-color" content="#ffcb05" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/slick/slick-theme.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:500&display=swap" rel="stylesheet">
    <style>
        /* carousel */

        * {
            box-sizing: border-box;
        }

        .center .slider {
            width: 100%;
            margin: 0px auto;



        }

        .center .slick-slide {
            margin: 0px 20px;

        }

        .center .slick-center {
            transform: scale(1.3);

        }

        .center .slick-active {
            padding: 20px 0;

        }

        .center .slide:not(.slick-active) {
            cursor: pointer;
        }

        .center .slick-slide:not(.slick-active) {
            margin: 20px 20px;
        }

        .center .slick-slide img {
            width: 100%;
            border-radius: 5px;
        }


        .center .slick-slide {
            transition: all ease-in-out .3s;
            opacity: .2;
        }

        .center .slick-active {
            opacity: .5;
        }

        .center .slick-current {
            opacity: 1;
        }

        @media (min-width: 580px) {
            * {
                box-sizing: border-box;
            }

            .center .slider {
                width: 100%;
                margin: 0px auto;



            }

            .center .slick-slide {
                margin: 0px 20px;

            }

            .center .slick-center {
                transform: scale(1.1);

            }

            .center .slick-active {
                padding: 20px 0;

            }

            .center .slide:not(.slick-active) {
                cursor: pointer;
            }

            .center .slick-slide:not(.slick-active) {
                margin: 20px 20px;
            }

            .center .slick-slide img {
                width: 100%;
                border-radius: 5px;
            }


            .center .slick-slide {
                transition: all ease-in-out .3s;
                opacity: .2;
            }

            .center .slick-active {
                opacity: .5;
            }

            .center .slick-current {
                opacity: 1;
            }

        }
    </style>
    <title><?php echo  $judul; ?></title>
</head>

<body>

    <!-- body div -->

    <div class="app">
        <div class="app-container">
    <!-- navbar -->
    <hr class="asdasd" style="margin-bottom: 35px;">
    <nav class="navbar navbar-light">
        <input class="form-control" style="width:70%; height: 35px; border-radius: 2px; border:none; background-color: #fff; margin-left: -8px; font-family: -apple-system,HelveticaNeue-Light,Helvetica Neue Light,Helvetica Neue,Helvetica,Roboto,Droid Sans,Arial,sans-serif; font-size: 15px; color: orange;" type="search" placeholder="Cari di ruangprint" aria-label="Search">
        <a href="keranjang2.html"><i class="fas fa-shopping-cart"></i></a>
        <a href=""><i class="fas fa-bell"></i></a>
        <a href="<?= base_url('dashboard/login'); ?>"><i class="fas fa-user"></i></a>
    </nav>
    <!-- akhir navbar -->

    <!-- navbar bottom -->
    <nav class="navbar-bottom navbar-light">
        <div class="container">
            <div class="row text-center">
                <div class="col">
                <a href="<?= base_url('dashboard/index');?>">
                <img src="<?= base_url() ?>assets/img/menu-bawah/home-2.png" alt="">
                <p class="menu-home">Home</p>
                </a>
                </div>
                <div class="col">
                <a href="<?= base_url('dashboard/login');?>">
                <img src="<?= base_url() ?>assets/img/menu-bawah/history-1.png" alt="">
                <p class="menu-history">History</p>
                </a>
                </div>
                <div class="col">
                <a href="<?= base_url('dashboard/login');?>">
                <img src="<?= base_url() ?>assets/img/menu-bawah/wallet-1.png" alt="">
                <p class="menu-wallet">Wallet</p>
                </a>
                </div>
                <div class="col">
                <a href="<?= base_url('dashboard/login');?>">
                <img src="<?= base_url() ?>assets/img/menu-bawah/profile-1.png" alt="">
                <p class="menu-profile">Profile</p>
                </a>
                </div>
            </div>
        </div>
        </nav>
    <!-- akhir navbar bottom -->

    <!-- slider slick -->
<section class="center slider" style="background-color: white;">
    <div>
      <img src="<?= base_url() ?>assets/img/banner-slide/free-ongkir1.png">
    </div>
    <div>
      <img src="<?= base_url() ?>assets/img/banner-slide/potongan-5001.png">
    </div>
    <div>
      <img src="<?= base_url() ?>assets/img/banner-slide/ramadhan1.png">
    </div>
    <div>
      <img src="<?= base_url() ?>assets/img/banner-slide/ramdhan-bundling1.png">
    </div>
  </section>
  <!-- akhir slider slick -->
  <!-- menu -->
  <div class="menu-icon" style="background-color: white;">
    <div class="container">
    <div class="row menu justify-content-center">
    <div class="col-xm-3">
        <figure class="figure">
        <a href="<?= base_url('produk/produk_indoor');?>"><img src="<?= base_url() ?>assets/img/menu/indoor.png"></a>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: 0px; text-align: center;">Ruang
        </figcaption>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: -2px; text-align: center;">Indoor
        </figcaption>
        </figure>

        <figure class="figure">
        <a href="<?= base_url('produk/produk_outdoor');?>"><img src="<?= base_url() ?>assets/img/menu/outdoor.png"></a>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: 0px; text-align: center;">Ruang
        </figcaption>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: -2px; text-align: center;">Outdoor
        </figcaption>
        </figure>

        <figure class="figure">
        <a href="<?= base_url('produk/produk_a3');?>"><img src="<?= base_url() ?>assets/img/menu/a3.png"></a>
         <figcaption class="figure-caption" style="font-size: 11px; margin-top: 0px; text-align: center;">Ruang
        </figcaption>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: -2px; text-align: center;">A3+
        </figcaption>
        </figure>

        <figure class="figure">
        <a href="<?= base_url('produk/produk_display');?>"><img src="<?= base_url() ?>assets/img/menu/display.png"></a>  
         <figcaption class="figure-caption" style="font-size: 11px; margin-top: 0px; text-align: center;">Ruang
        </figcaption>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: -2px; text-align: center;">Display
        </figcaption>
        </figure>

    </div>
    <div class="col-xm-3">
        <figure class="figure">
        <a href=""><img src="<?= base_url() ?>assets/img/menu/t-shirt.png"></a>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: 0px; text-align: center;">Ruang
        </figcaption>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: -2px; text-align: center;">T-Shirt
        </figcaption>
        </figure>
        
        <figure class="figure">
        <a href=""><img src="<?= base_url() ?>assets/img/menu/marchandise.png"></a>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: 0px; text-align: center;">Ruang
        </figcaption>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: -2px; text-align: center;">Marchandise
        </figcaption>
        </figure>

        <figure class="figure">
        <a href=""><img src="<?= base_url() ?>assets/img/menu/kanvas.png"></a>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: 0px; text-align: center;">Ruang
        </figcaption>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: -2px; text-align: center;">Kanvas
        </figcaption>
        </figure>

        <figure class="figure">
        <a href=""><img src="<?= base_url() ?>assets/img/menu/chat.png"></a> 
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: 0px; text-align: center;">Ruang
        </figcaption>
        <figcaption class="figure-caption" style="font-size: 11px; margin-top: -2px; text-align: center;">Chat
        </figcaption>
        </figure>
    </div>
    </div>
    </div>
  </div>
  <!-- akhir menu -->

<!-- banner iklan -->
    <img src="<?= base_url() ?>assets/img/banner-iklan/promo-kejutan1.png" style="width:100%; margin-top: -20px;" alt="cashback 500ribu">
<!-- akhir banner iklan -->

<!-- judul kategori -->
    <div class="title-kategori clearfix" style="background-color: white;">
        <div class="float-left" style="font-size: 14px;font-weight: bold; color: orange; font-family: -apple-system,HelveticaNeueMedium,HelveticaNeue-Medium,Helvetica Neue Medium,Helvetica Neue,Roboto,Droid Sans,Arial Bold,Arial,sans-serif; padding-left: 10px;padding-top: 10px;">
            PRODUK PROMO
        </div>
        <div class="float-right" style="font-size: 11px; color: #eee; font-family: -apple-system,HelveticaNeueMedium,HelveticaNeue-Medium,Helvetica Neue Medium,Helvetica Neue,Roboto,Droid Sans,Arial Bold,Arial,sans-serif; padding-right: 10px;padding-top: 13px;">
            <a href="<?= base_url('produk/produk_promo')?>" style="color: #acacac; ">Lihat Lainnya -- 
            <i class="fas fa-chevron-right"></i></a>
        </div>
    </div>
<!-- akhir judul -->

<!-- produk promo -->
    
    <div class="produk-promo">
        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk1">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">

            <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
    </a> 
    <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk2">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>
        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk3">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>
        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk4">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>
        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk5">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>
        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk6">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>
        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk7">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>
    </div>
<!-- akhir produk promo -->


<!-- produk PAKET CETAK -->

<!-- judul kategori -->
    <div class="title-kategori clearfix" style="background-color: white;">
        <div class="float-left" style="font-size: 14px;font-weight: bold; color: orange; font-family: -apple-system,HelveticaNeueMedium,HelveticaNeue-Medium,Helvetica Neue Medium,Helvetica Neue,Roboto,Droid Sans,Arial Bold,Arial,sans-serif; padding-left: 10px;padding-top: 10px;">
            PAKET CETAK
        </div>
        <div class="float-right" style="font-size: 11px; color: #eee; font-family: -apple-system,HelveticaNeueMedium,HelveticaNeue-Medium,Helvetica Neue Medium,Helvetica Neue,Roboto,Droid Sans,Arial Bold,Arial,sans-serif; padding-right: 10px;padding-top: 13px;">
            <a href="<?= base_url('produk/produk_paket')?>" style="color: #acacac; ">Lihat Lainnya -- 
            <i class="fas fa-chevron-right"></i></a>
        </div>
    </div>
<!-- akhir judul -->

<!-- produk paket cetak -->
    
    <div class="paket-cetak">
            <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk1">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">

            <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>

        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk2">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>

        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk3">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>

        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk4">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>

        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk5">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>

        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk6">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>

        <a href="<?= base_url('produk/produk_deskripsi');?>">
        <div class="produk7">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        </a>
    </div>
   
<!-- akhir produk cetak -->

<!-- ruang info -->
<!-- judul kategori -->
    <div class="title-kategori clearfix" style="background-color: white;">
        <div class="float-left" style="font-size: 14px;font-weight: bold; color: orange; font-family: -apple-system,HelveticaNeueMedium,HelveticaNeue-Medium,Helvetica Neue Medium,Helvetica Neue,Roboto,Droid Sans,Arial Bold,Arial,sans-serif; padding-left: 10px;padding-top: 10px;">
            RUANG INFO
        </div>
        <div class="float-right" style="font-size: 11px; color: #eee; font-family: -apple-system,HelveticaNeueMedium,HelveticaNeue-Medium,Helvetica Neue Medium,Helvetica Neue,Roboto,Droid Sans,Arial Bold,Arial,sans-serif; padding-right: 10px;padding-top: 13px;">
            <a href="Lihatlainnya.html" style="color: #acacac; ">Lihat Lainnya -- 
            <i class="fas fa-chevron-right"></i></a>
        </div>
    </div>
<!-- akhir judul -->

<!-- section ruang info -->
    
    <div class="ruang-info">
        <div class="produk1">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">

            <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        <div class="produk2">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        <div class="produk3">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        <div class="produk4">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        <div class="produk5">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        <div class="produk6">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
        <div class="produk7">
            <figure class="figure">
            <img src="<?= base_url() ?>assets/img/produk-promo/Paket-Bundling-1.png">
             <figcaption class="figure-caption" style=" color:black;">Paket Bundling 1
            </figcaption>
             <figcaption class="figure-caption" style="font-weight: bold; color:red;">Rp 3.500.000
            </figcaption>
            <figcaption class="figure-caption" style="font-size: 12px; font-weight: bold; color:black"><del>Rp 3.500.000</del>
            </figcaption>
            </figure>
        </div>
    </div>
   
<!-- akhir ruang info -->
</div>
 </div>

<!-- akhir body div -->
   <!-- Optional JavaScript -->
   <!-- jQuery first, then Popper.js, then Bootstrap JS -->

   <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
   <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

   <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
   <script src="<?= base_url() ?>assets/slick/slick.js" type="text/javascript" charset="utf-8"></script>

   <script type="text/javascript">
       $(document).on('ready', function() {
           $('.center').slick({
               centerMode: true,
               centerPadding: '60px',
               slidesToShow: 1,
               focusOnSelect: true,
               autoplay: true,
               responsive: [{
                       breakpoint: 768,
                       settings: {
                           arrows: false,
                           centerMode: true,
                           centerPadding: '40px',
                           slidesToShow: 1
                       }
                   },
                   {
                       breakpoint: 480,
                       settings: {
                           arrows: false,
                           centerMode: true,
                           centerPadding: '40px',
                           slidesToShow: 1

                       }
                   }
               ]
           });

       });
   </script>


   </body>

   </html>