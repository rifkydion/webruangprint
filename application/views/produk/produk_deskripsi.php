<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
    <meta name="theme-color" content="#ffcb05" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>css/style-detail-satuan.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:500&display=swap" rel="stylesheet">

    <title><?php echo $judul; ?></title>
</head>

<body>
    <div class="app">
        <div class="app-container">
            <!-- navbar -->

            <nav class="navbar navbar-light" style=" width: 100%; height: 50px; position: absolute; z-index: 2;  background-image: linear-gradient(to top, rgba(255,0,0,0), rgb(221, 221, 221));">
                <a href="<?= base_url('dashboard/index') ?>"><i class="fas fa-arrow-left"></i></a>
                <a href="keranjang2.html"><i class="fas fa-shopping-cart"></i></a>
            </nav>

            <nav class="navbar-bottom shadow-lg text-center">

                <a href="order-produk-satuan.html"><button type="button" class="btn btn-beli ">Beli</button></a>

                <button type="button" class="btn btn-tambahkeranjang">Tambah Keranjang</button>
            </nav>

            <!-- slide show -->
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="<?= base_url('assets/'); ?>img/slide-detail/poster.png" class="d-block w-100" alt="poster">
                    </div>
                    <div class="carousel-item">
                        <img src="<?= base_url('assets/'); ?>img/slide-detail/x-banner.png" class="d-block w-100" alt="x banner">
                    </div>
                    <div class="carousel-item">
                        <img src="<?= base_url('assets/'); ?>img/slide-detail/rollbanner.png" class="d-block w-100" alt="roll banner">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <!-- akhir slide -->

            <!-- tombolwhislist -->

            <div class="like-content">
                <button class="btn-secondary like-review">
                    <i class="fa fa-heart" aria-hidden="true"></i>
                </button>
            </div>

            <!-- akhir tombol whislist -->

            <!-- detailproduk -->
            <div class="container shadow-sm" style="background-color: white;
            width:100%; height: 70px; margin-bottom: 10px;">
                <h6 class="nama-produk">Promo Paket Bundling Kit 1</h6>
                <div class="harga-produk">Rp 5.000.000</div>
            </div>

            <div class="container shadow-sm" style="background-color: white;
            width:100%; height: 110px; margin-bottom: 10px;">
                <div class="free-ongkir">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="keterangan">Free Ongkir Jarak 5Km</div>
                        </div>
                        <div class="col-sm-12">
                            <img src="img/icon-store.png" alt="">
                            <span class="alamat-kantor">dari<strong> Kantor ruangprint</strong></span>
                        </div>
                        <div class="col-sm-12">
                            <img src="img/delivery-free.png" alt="">
                            <span class="jarak-kirim">
                                <strong>Kurir ruangprint</strong>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container shadow-sm" style="background-color: white;
            width:100%; height: 150px; margin-bottom: 60px;">
                <div class="info-detail">
                    <div class="row">
                        <div class="col">
                            <h6 class="informasi-produk">Informasi Produk</h6>
                            <div class="bahan-produk">Bahan</div>
                            <div class="bahan-produk">Material</div>
                            <div class="bahan-produk">Ukuran</div>
                            <div class="bahan-produk">Min. Pemesanan</div>
                        </div>
                        <div class="col text-right">
                            <h6 class="detail-bahan"></h6>
                            <div class="bahan-produk">Albatros</div>
                            <div class="bahan-produk">X-Banner</div>
                            <div class="bahan-produk">60x160cm</div>
                            <div class="bahan-produk">1</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- akhir detail produk -->
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>

    <script type="text/javascript">
        $(function() {
            $(document).on('click', '.like-review', function(e) {
                $(this).html('<i class="fa fa-heart" aria-hidden="true"></i>');
                $(this).children('.fa-heart').addClass('animate-like');
            });
        });
    </script>

</body>

</html>