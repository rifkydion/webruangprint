<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
    <meta name="theme-color" content="#ffcb05" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:500&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>css/style-produk.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <title><?php echo $judul; ?></title>
</head>

<body>
    <div class="app">
        <div class="app-container">
            <!-- navbar -->

            <nav class="navbar navbar-light" style=" width: 100%; background-color: #ffcb05;">
                <a href="<?= base_url('dashboard/index'); ?>"><i class="fas fa-arrow-left"></i></a>
                <input type="search" action="#indoor" method="get" id="simpleTable" class="form-control" style="margin-left: 0px; width:75%; height: 35px; border-radius: 2px; border:none; background-color: #fff; font-family: -apple-system,HelveticaNeue-Light,Helvetica Neue Light,Helvetica Neue,Helvetica,Roboto,Droid Sans,Arial,sans-serif; font-size: 15px; color: orange;" placeholder="Produk Indoor..." aria-controls="sampleTable">
                <a href=""><i class="fas fa-shopping-cart"></i></a>
            </nav>

            <div class="indoor" id="indoor" name="indoor">
                <div class="list-group">

                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button type="button" class="btn btn-warning">Albatros<span class="badge badge-danger">15%</span>
                        </button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Sticker Ritrama</button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Sticker Transparant</button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Photopaper</button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Duratran</button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Duratran Overprint</button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Ritrama
                            Overprint</button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Sticker Vinyl</button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Sunblast</button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Flexy Korea 440gr</button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Flexy 340gr</button></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex flex-row-reverse"><button class="btn btn-warning">Kanvas Art</button></a>
                </div>
            </div>

        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $('#sampleTable').DataTable();
    </script>
</body>

</html>