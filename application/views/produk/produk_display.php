<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
    <meta name="theme-color" content="#ffcb05" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/'); ?>css/lihatlainnya.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/') ?>slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/') ?>slick/slick-theme.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:500&display=swap" rel="stylesheet">

    <title><?php echo $judul; ?></title>
</head>

<body>

    <!-- body div -->

    <div class="app">
        <div class="app-container">
            <!-- navbar -->
            <hr class="asdasd" style="margin-bottom: 35px;">
            <nav class="navbar">
                <div class="tombol-back">
                    <a href="<?= base_url('dashboard/index'); ?>"><i class="fas fa-arrow-left"></i></a>
                </div>
                <input class="form-control" style="width:75%; height: 35px; border-radius: 2px; border:none; background-color: rgb(248, 248, 248); margin-left: 0px; font-family: -apple-system,HelveticaNeue-Light,Helvetica Neue Light,Helvetica Neue,Helvetica,Roboto,Droid Sans,Arial,sans-serif; font-size:rgb(180, 180, 180)255, 255, 255);" type="search" placeholder="Cari di ruangprint" aria-label="Search">
                <a href="keranjang2.html"><i class="fas fa-shopping-cart"></i></a>
            </nav>
            <!-- akhir navbar -->
            <div class="container">
                <div class="row text-center">
                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                    <div class="col">
                        <a href="<?= base_url('produk/produk_deskripsi'); ?>">
                            <div class="produk">
                                <figure class="figure">
                                    <img src="<?= base_url('assets/') ?>img/produk-promo/Paket-Bundling-1.png">
                                    <figcaption class="figure-caption" style=" color:black; font-size: 13px;margin-bottom: 10px; margin-top:5px;">
                                        Paket
                                        Roll Up Banner 60x160cm
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-weight: bold; color:red; font-size: 13px;">Rp
                                        3.500.000
                                    </figcaption>
                                    <figcaption class="figure-caption" style="font-size: 11px; color:black">
                                        <del>Rp 3.500.000</del>
                                    </figcaption>
                                </figure>
                            </div>
                        </a>
                    </div>

                </div>
            </div>



        </div>
    </div>

    <!-- akhir body div -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="<?= base_url('assets/') ?>slick/slick.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>