<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['judul'] = 'Ruangprint | Digital Printing & Advertising';
        $data['tb_customer'] = $this->db->get_where('tb_customer', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('user/index', $data);
    }

    public function profile()
    {
        $data['judul'] = 'Ruangprint | My Profile';
        $data['tb_customer'] = $this->db->get_where('tb_customer', ['email' =>
        $this->session->userdata('email')])->row_array();
        $this->load->view('user/profile', $data);
    }

    public function editprofile()
    {
        $data['tb_customer'] = $this->db->get_where('tb_customer', ['email' =>
        $this->session->userdata('email')])->row_array();
        $data['judul'] = 'Ruangprint | Ubah Data';

        $this->form_validation->set_rules('name', 'Full Name', 'required|trim', [
            'required' => 'Email tidak boleh kosong'
        ]);
        $this->form_validation->set_rules('telp', 'Telp', 'required|trim', [
            'required' => 'Nomor telephone tidak boleh kosong'

        ]);
        if ($this->form_validation->run() == false) {
            $this->load->view('user/editprofile', $data);
        } else {
            $name = $this->input->post('name');
            $no_telp = $this->input->post('telp');
            $email = $this->input->post('email');

            // cek jika ada gambar
            $upload_image = $_FILES['photo']['nm_customer'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['upload_path'] = './assets/img/profile/';
                $config['max_width'] = '1024';
                $config['max_height'] = '1024';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('photo')) {
                    $old_image = $data['tb_customer']['photo'];
                    if ($old_image != 'default.jpg') {
                        unlink(FCPATH . 'assets/img/profile/' . $old_image);
                    }
                    $new_image = $this->upload->data('file_name');
                    $this->db->set('photo', $new_image);
                } else {
                    echo $this->upload->display_errors();
                }
            }

            $this->db->set('nm_customer', $name);
            $this->db->set('no_telp', $no_telp);
            $this->db->where('email', $email);
            $this->db->update('tb_customer');


            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Profile Kamu Sudah Di Update!</div>');
            redirect('user/profile');
        }
    }
}
