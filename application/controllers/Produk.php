<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends CI_Controller
{
    public function produk_promo()
    {

        $data['judul'] = 'Ruangprint | Ruang Promo';
        $this->load->view('produk/produk_promo', $data);
    }

    public function produk_paket()
    {
        $data['judul'] = 'Ruangprint | Ruang Paket';
        $this->load->view('produk/produk_paket', $data);
    }

    public function produk_indoor()
    {

        $data['judul'] = 'Ruangprint | Ruang Indoor';
        $this->load->view('produk/produk_indoor', $data);
    }

    public function produk_outdoor()
    {
        $data['judul'] = 'Ruangprint | Ruang Outdoor';
        $this->load->view('produk/produk_outdoor', $data);
    }
    public function produk_a3()
    {
        $data['judul'] = 'Ruangprint | Ruang A3+';
        $this->load->view('produk/produk_a3', $data);
    }

    public function produk_display()
    {

        $data['judul'] = 'Ruangprint | Ruang Display';
        $this->load->view('produk/produk_display', $data);
    }

    public function produk_deskripsi()
    {

        $data['judul'] = 'Ruangprint | Deskripsi Produk Promo';
        $this->load->view('produk/produk_deskripsi', $data);
    }
}
