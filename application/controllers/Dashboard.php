<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('encript_helper');
    }

    public function index()
    {
        if ($this->session->userdata('email')) {
            redirect('dashboard/login');
        }
        $data['judul'] = 'Ruangprint | Digital Printing & Advertising';
        $this->load->view('dashboard/index', $data);
    }

    public function login()
    {
        if ($this->session->userdata('email')) {
            redirect('user');
        }

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Email anda salah'
        ]);
        $this->form_validation->set_rules('password', 'Password', 'required', [
            'required' => 'Password tidak boleh kosong'
        ]);
        if ($this->form_validation->run() == false) {
            $data['judul'] = 'Login Page';
            $data['judul'] = 'Ruangprint | Login Akun';
            $this->load->view('dashboard/login', $data, $data);
        } else {
            // validasinya success
            $this->_login();
        }
    }


    private function _login()
    {
        $email = $this->input->post('email');
        $pass = $this->input->post('password');
        $password     = encrypt($pass);

        $user = $this->db->get_where('tb_customer', ['email' => $email])->row_array();
        //jika usernya ada
        if ($user) {
            if ($password == $user['password']) {
                $data = [
                    'email' => $user['email'],
                    'role_id' => $user['role_id']
                ];
                $this->session->set_userdata($data);
                redirect('user');
            } else {
                // $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password anda salah '.decrypt($user['password']).$user[email].'</div>');
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password anda salah  ' . $user['password'] . $password . '</div>');
                redirect('dashboard/login');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email belum pernah terdaftar</div>');
            redirect('dashboard/login');
        }
    }

    public function register()
    {

        if ($this->session->userdata('email')) {
            redirect('user');
        }

        $this->form_validation->set_rules('password2', 'Password2', 'required|trim|matches[password]');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[6]|matches[password2]', [
            'matches' => 'Password tidak sama!',
            'min_length' => 'Password terlalu pendek!',
            'required' => 'Password tidak boleh kosong'
        ]);
        $this->form_validation->set_rules('telp', 'Telp', 'required|trim|is_unique[user.no_telp]', [
            'required' => 'Nomor telephone tidak boleh kosong',
            'is_unique' => 'Nomor anda sudah terdaftar'
        ]);
        $this->form_validation->set_rules('name', 'Name', 'required|trim', [
            'required' => 'Nama tidak boleh kosong'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
            'required' => 'Email tidak boleh kosong',
            'valid_email' => 'Email anda salah',
            'is_unique' => 'Email anda sudah terdaftar'
        ]);

        if ($this->form_validation->run() == false) {
            $data['judul'] = 'Ruangprint | register Akun';
            $this->load->view('dashboard/register', $data);
        } else {
            $data = [
                'name' => htmlspecialchars($this->input->post('name', true)),
                'email' => htmlspecialchars($this->input->post('email', true)),
                'no_telp' => htmlspecialchars($this->input->post('telp', true)),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                'image' => 'default.jpg',
                'role_id' => 1,
                'is_active' => 1,
                'date_created' => time()

            ];

            $this->db->insert('user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Selamat akun anda sudah terdaftar. Silahkan Login</div>');
            redirect('dashboard/login');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('role_id');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Kok Lu Keluar CUKK! Masuk lagi BEGO!</div>');
        redirect('dashboard/login');
    }
}
